import { expect, tap } from '@pushrocks/tapbundle';
import * as cardano from '../ts/index';

let testCardanoApiInstance: cardano.CardanoApi;

tap.skip.test('should start cardano node', async () => {
  await cardano.CardanoApi.startCardanoNodeAndWalletWithDockerCompose();
});

tap.test('should create a CardanoApi instance', async (tools) => {
  testCardanoApiInstance = await cardano.CardanoApi.createForDockerCompose();
});

tap.test('should create a new wallet', async () => {
  const wallet = await testCardanoApiInstance.createWallet('myWallet');
})

tap.skip.test('should stop cardano-node', async () => {
  await cardano.CardanoApi.stopCardanoNodeAndWalletWithDockerCompose();
});

tap.start();
