import * as plugins from './cardano.plugins';

export const packageDir = plugins.path.join(__dirname, '../');
export const assetsDir = plugins.path.join(packageDir, './assets');
export const nogitDir = plugins.path.join(packageDir, './.nogit/');
