import * as plugins from './cardano.plugins';
import * as paths from './cardano.paths';
import { defaultCardanoDockerCompose } from './cardano.classes.cardanodockercompose';
import { Serializable } from 'child_process';
import { CardanoWallet } from './cardano.classes.cardanowallet';

export class CardanoApi {
  // STATIC
  public static async startCardanoNodeAndWalletWithDockerCompose() {
    await defaultCardanoDockerCompose.start();
  }

  public static async stopCardanoNodeAndWalletWithDockerCompose() {
    await defaultCardanoDockerCompose.stop();
  }

  public static async checkForRunningWalletApi() {
    const instance = CardanoApi.createForDockerCompose();
  }

  public static async createForDockerCompose() {
    const cardanoApiInstance = new CardanoApi();
    await cardanoApiInstance.isSynced();
    return cardanoApiInstance;
  }

  // INSTANCE
  constructor() {}

  public async isSynced(): Promise<boolean> {
    const response = await this.request('/network/information', 'GET');
    console.log(response);
    return true;
  }

  /**
   * creates a wallet
   */
  public async createWallet(walletNameArg: string) {
    const newWallet = await CardanoWallet.createNewWallet(this, walletNameArg);
    return newWallet;
  }

  public async request(routeArg: string, methodArg: 'GET' | 'POST', payloadArg?: Serializable) {
    const response = await plugins.smartrequest.request('http://localhost:8090/v2' + routeArg, {
      method: methodArg,
      keepAlive: false,
      headers: {
        'content-type': 'application/json'
      },
      requestBody: payloadArg
    });
    return response.body;
  }
}
