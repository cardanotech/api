import * as plugins from '../cardano.plugins';
import * as paths from '../cardano.paths';

export const getRandomMnemonic = async (): Promise<string[]> => {
  
  const entropy = Buffer.from(randomHex(), 'hex');
  const wordlist = await getWordlist();

  // 128 <= ENT <= 256
  if (entropy.length < 16) {
    throw new TypeError(`The entropy is too small!`);
  }
  if (entropy.length > 32) {
    throw new TypeError(`The entropy is too big!`);
  }
  if (entropy.length % 4 !== 0) {
    throw new TypeError('The entropy must be devidable by 4');
  }

  const entropyBits = bytesToBinary(Array.from(entropy));
  const checksumBits = deriveChecksumBits(entropy);

  const bits = entropyBits + checksumBits;
  const chunks = bits.match(/(.{1,11})/g)!;
  const words = chunks.map((binary: string): string => {
    const index = binaryToByte(binary);
    return wordlist![index];
  });

  return words;
};

const getWordlist = async () => {
  const wordlistString = plugins.smartfile.fs.toStringSync(plugins.path.join(paths.assetsDir, './dictionary.txt'));
  const wordlistArray = wordlistString.split('\n');
  return wordlistArray;
};

const randomHex = () => {
  const S = 'abcdefABCDEF0123456789';
  const N = 41;
  return Array.from(Array(N))
    .map(() => S[Math.floor(Math.random() * S.length)])
    .join('');
};

const deriveChecksumBits = (entropyBuffer: Buffer): string => {
  const ENT = entropyBuffer.length * 8;
  const CS = ENT / 32;
  const hash = plugins.createHash('sha256').update(entropyBuffer).digest();

  return bytesToBinary(Array.from(hash)).slice(0, CS);
};

const binaryToByte = (bin: string): number => parseInt(bin, 2);

const bytesToBinary = (bytes: number[]): string =>
  bytes.map((x: number): string => lpad(x.toString(2), '0', 8)).join('');

const lpad = (str: string, padString: string, length: number): string => {
  while (str.length < length) str = padString + str;
  return str;
};
