import { CardanoApi } from './cardano.classes.cardanoapi';
import * as plugins from './cardano.plugins';
import * as helpers from './helpers';

/**
 * A Cardano Wallet
 */
export class CardanoWallet {
  // STATIC
  public static async createNewWallet(cardanoApiRef: CardanoApi, walletNameArg: string) {
    const seed = await helpers.getRandomMnemonic();
    // console.log(seed);
    const response = await cardanoApiRef.request('/wallets', 'POST', {
      name: walletNameArg,
      mnemonic_sentence: seed,
      passphrase: 'cardanotech-api',
    });
    console.log(response);
  }

  'passphrase': { last_updated_at: '2021-03-01T02:32:14.226629408Z' };
  'address_pool_gap': 20;
  'state': { status: 'syncing'; progress: { quantity: 29.65; unit: 'percent' } };
  'balance': {
    reward: { quantity: 0; unit: 'lovelace' };
    total: { quantity: 0; unit: 'lovelace' };
    available: { quantity: 0; unit: 'lovelace' };
  };
  'name': 'myWallet';
  'delegation': { next: []; active: { status: 'not_delegating' } };
  'id': '3bb741acf03e536161ac6d0a9ad7db23e4a40ad1';
  'tip': {
    height: { quantity: 1605926; unit: 'block' };
    time: '2018-09-30T17:33:11Z';
    epoch_number: 74;
    absolute_slot_number: 1606285;
    slot_number: 7885;
  };
  'assets': { total: []; available: [] };
}
