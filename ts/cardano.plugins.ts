// node native
import * as fs from 'fs';
import * as path from 'path';

export { fs, path };

// pushrocks scope
import * as smartfile from '@pushrocks/smartfile';
import * as smarthash from '@pushrocks/smarthash';
import * as smartlog from '@pushrocks/smartlog';
import * as smartrequest from '@pushrocks/smartrequest';
import * as smartshell from '@pushrocks/smartshell';

export { smartfile, smarthash, smartlog, smartshell, smartrequest };

// third party scope
import createHash from 'create-hash';
import * as dockerCompose from 'docker-compose';

export { createHash, dockerCompose };
